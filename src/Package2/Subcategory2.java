package Package2;
import java.util.ArrayList;
/**
 * Created by Home on 25.08.2017.
 */
public class Subcategory2 extends Category{
  public static void main(String[] args) {
    Subcategory2 subcategory2=new Subcategory2();
    subcategory2.getName();
    subcategory2.getQty();
  }

  @Override
  public String getName() {
    ArrayList<String> subcat = new ArrayList<String>();
    subcat.add("Audio");
    subcat.add("Video");
    subcat.add("Games");
    System.out.println(subcat);
    return null;
  }

  @Override
  int getQty() {
    int qtySp=20;
    int par=11;
    int single=8;
    int sum=qtySp+par+single;
    System.out.println(sum);
    return sum;
  }
}
